import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:youth_flutter_final_project/firebase/fb_auth_controller.dart';
import 'package:youth_flutter_final_project/models/admin.dart';
import 'package:youth_flutter_final_project/models/bills.dart';
import 'package:youth_flutter_final_project/models/category.dart';
import 'package:youth_flutter_final_project/models/customers.dart';
import 'package:youth_flutter_final_project/models/distributor.dart';
import 'package:youth_flutter_final_project/models/order.dart';
import 'package:youth_flutter_final_project/models/product.dart';

class FbFireStoreController {
  FirebaseFirestore _firebaseFireStore = FirebaseFirestore.instance;

  // *********************** Admin ************************* //

  Future<bool> addAdmin({required Admin admin}) async {
    return await _firebaseFireStore
        .collection('Admin')
        .add(admin.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> isAdmin(String email) async {
    return _firebaseFireStore
        .collection('Admin')
        .where('email', isEqualTo: email)
        .get()
        .then((value) {
      if (value.docs.length > 0) {
        return true;
      } else {
        return false;
      }
    });
  }

  Stream<QuerySnapshot> readAdmin() async* {
    yield* _firebaseFireStore.collection('Admin').where('email', isEqualTo: FbAuthController().user.email).snapshots();
  }

  Future<String> getAdminName() async {
    return _firebaseFireStore
        .collection('Admin')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }
  Future<String> getAdminId() async {
    return _firebaseFireStore
        .collection('Admin')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].id);
  }
  Future<bool> updateAdminImage(
      {required String path, required String image}) async {
    return await _firebaseFireStore
        .collection('Admin')
        .doc(path)
        .update({'image' : image})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAdminName(
      {required String path, required String name}) async {
    return await _firebaseFireStore
        .collection('Admin')
        .doc(path)
        .update({'name' : name})
        .then((value) => true)
        .catchError((error) => false);
  }

  // *********************** Distributor ************************* //

  Future<bool> addDistributor({required Distributor distributor}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .add(distributor.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateDistributor(
      {required String path, required Distributor distributor}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .doc(path)
        .update(distributor.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteDistributor({required String path}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readDistributor() async* {
    yield* _firebaseFireStore.collection('Distributor').snapshots();
  }

  Future<String> getDistributorName() async {
    return _firebaseFireStore
        .collection('Distributor')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }

  // *********************** Customer ************************* //

  Future<bool> addCustomer({required Customers customer}) async {
    return await _firebaseFireStore
        .collection('Customer')
        .add(customer.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateCustomer(
      {required String path, required Customers customer}) async {
    return await _firebaseFireStore
        .collection('Customer')
        .doc(path)
        .update(customer.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteCustomer({required String path}) async {
    return await _firebaseFireStore
        .collection('Customer')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readCustomer() async* {
    yield* _firebaseFireStore.collection('Customer').snapshots();
  }

  Future<List<String>> getCustomerName() async {
    return await _firebaseFireStore.collection('Customer').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('name').toString());
      }
      return names;
    });
  }

  Future<List<String>> getMarketName() async {
    return await _firebaseFireStore.collection('Customer').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('marketName').toString());
      }
      return names;
    });
  }

  Future<String> getCustomerId(String name) async {
    return _firebaseFireStore
        .collection('Customer')
        .where('marketName', isEqualTo: name)
        .get()
        .then((value) => value.docs[0].id);
  }

  // *********************** Order Collection *********************** //

  Future<bool> addOrder({required Order order}) async {
    return await _firebaseFireStore
        .collection('Order')
        .add(order.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateOrder({required String path, required Order order}) async {
    return await _firebaseFireStore
        .collection('Order')
        .doc(path)
        .update(order.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteOrder({required String path}) async {
    return await _firebaseFireStore
        .collection('Order')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readOrder() async* {
    yield* _firebaseFireStore.collection('Order').snapshots();
  }

  Stream<QuerySnapshot> readUserOrder(String name) async* {
    yield* _firebaseFireStore
        .collection('Order')
        .where('name', isEqualTo: name)
        .snapshots();
  }

  // *********************** bills Collection *********************** //

  Future<bool> addBill({required Bill bill}) async {
    return await _firebaseFireStore
        .collection('bill')
        .add(bill.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteBill({required String path}) async {
    return await _firebaseFireStore
        .collection('bill')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readBill() async* {
    yield* _firebaseFireStore.collection('bill').snapshots();
  }

  // *********************** Counter Collection *********************** //

  Future<int> readCustomerCount() async {
    return await _firebaseFireStore.collection('Customer').get().then((value) {
      return value.docs.length;
    });
  }

  Future<int> readEmployeeCount() async {
    return await _firebaseFireStore
        .collection('Distributor')
        .get()
        .then((value) {
      return value.docs.length;
    });
  }

  Future<int> readOrderCount() async {
    return await _firebaseFireStore.collection('Order').get().then((value) {
      return value.docs.length;
    });
  }

  Future<int> readProductCount() async {
    return await _firebaseFireStore.collection('product').get().then((value) {
      return value.docs.length;
    });
  }

  Future<int> readBillsCount() async {
    return await _firebaseFireStore.collection('bill').get().then((value) {
      return value.docs.length;
    });
  }

  Stream<QuerySnapshot> readCategory() async* {
    yield* _firebaseFireStore
        .collection('category')
        .orderBy('categoryName', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> readProduct() async* {
    yield* _firebaseFireStore
        .collection('product')
        .orderBy('nameProduct', descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> readCustomProduct(String category) async* {
    yield* _firebaseFireStore
        .collection('product')
        .where('categoryId', isEqualTo: category)
        .snapshots();
  }

  Future<bool> createCategory({required Category category}) async {
    return await _firebaseFireStore
        .collection('category')
        .add(category.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> createProduct({required Product product}) async {
    return await _firebaseFireStore
        .collection('product')
        .add(product.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateCategory(
      {required String path, required Category category}) async {
    return await _firebaseFireStore
        .collection('category')
        .doc(path)
        .update(category.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateProduct(
      {required String path, required Product product}) async {
    return await _firebaseFireStore
        .collection('product')
        .doc(path)
        .update(product.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<List<String>> getProductName() async {
    return await _firebaseFireStore.collection('product').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('nameProduct').toString());
      }
      return names;
    });
  }

  Future<List<String>> getProductId(String id) async {
    return await _firebaseFireStore.collection('product').where('categoryId', isEqualTo: id).get().then((value) {
      List<String> ids = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        ids.add(value.docs[i].id);
      }
      return ids;
    });
  }

  Future<List<String>> getBillId(String id) async {
    return await _firebaseFireStore.collection('bill').where('marketId', isEqualTo: id).get().then((value) {
      List<String> ids = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        ids.add(value.docs[i].id);
      }
      return ids;
    });
  }

  Future<List<String>> getCustomId(String id) async {
    return await _firebaseFireStore.collection('product').where('categoryId', isEqualTo: id).get().then((value) {
      List<String> ids = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        ids.add(value.docs[i].id);
      }
      return ids;
    });
  }

  Future<List<DocumentSnapshot>> getCustomProduct(String path) async {
    return await _firebaseFireStore.collection('category').where('categoryId', isEqualTo: path).get().then((value) => value.docs);}

  Future<String> getCategoryName(String id) async {
    return await _firebaseFireStore.collection('category').where(FieldPath.documentId, isEqualTo: id).get().then((value) => value.docs[0].get('categoryName'));
  }

  Future<List<String>> getProductPrice() async {
    return await _firebaseFireStore.collection('product').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('price').toString());
      }
      return names;
    });
  }

  Future<bool> deleteCategory({required String path}) async {
   return await _firebaseFireStore
        .collection('category')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteProduct({required String path}) async {
    return await _firebaseFireStore
        .collection('product')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

}
