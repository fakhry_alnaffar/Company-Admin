class Distributor{
  String name = '';
  String carNumber = '';
  String typeCar = '';
  String mobileNumber = '';
  String email = '';
  String path = '';
  String image = '';

  Distributor();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['carNumber'] = carNumber;
    map['typeCar'] = typeCar;
    map['mobileNumber'] = mobileNumber;
    map['email'] = email;
    map['image'] = image;
    return map;
  }
}