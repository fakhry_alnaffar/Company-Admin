import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:youth_flutter_final_project/preferences/app_preferences.dart';
import 'package:youth_flutter_final_project/screen/auth/change_password.dart';
import 'package:youth_flutter_final_project/screen/auth/foreget_password_screen.dart';
import 'package:youth_flutter_final_project/screen/auth/home_screen.dart';
import 'package:youth_flutter_final_project/screen/auth/launch_screen.dart';
import 'package:youth_flutter_final_project/screen/auth/login_screen.dart';
import 'package:youth_flutter_final_project/screen/auth/sign_up_screen.dart';
import 'package:youth_flutter_final_project/screen/bills/add__num3_bill.dart';
import 'package:youth_flutter_final_project/screen/bills/add__num4_bill.dart';
import 'package:youth_flutter_final_project/screen/bills/add_bill_num1.dart';
import 'package:youth_flutter_final_project/screen/bills/add_bill_num2.dart';
import 'package:youth_flutter_final_project/screen/bills/finish_bills.dart';
import 'package:youth_flutter_final_project/screen/bills/show_bills.dart';
import 'package:youth_flutter_final_project/screen/customer/customer_screen.dart';
import 'package:youth_flutter_final_project/screen/distributors/distributors_screen.dart';
import 'package:youth_flutter_final_project/screen/order/order_show.dart';
import 'package:youth_flutter_final_project/screen/product/add_category.dart';
import 'package:youth_flutter_final_project/screen/product/add_product.dart';
import 'package:youth_flutter_final_project/screen/product/category_home.dart';
import 'package:youth_flutter_final_project/screen/profile/profile_screen.dart';
import 'package:youth_flutter_final_project/screen/setting/setting_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppPreferences().initPreferences();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        floatingActionButtonTheme:
            FloatingActionButtonThemeData(backgroundColor: Color(0xff5A55CA)),
        scaffoldBackgroundColor: Colors.white,
        appBarTheme: AppBarTheme(
          titleSpacing: 20,
          centerTitle: false,
          iconTheme: IconThemeData(color: Colors.white),
          elevation: 0.0,
          backgroundColor: Colors.white,
          backwardsCompatibility: false,
          titleTextStyle: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          systemOverlayStyle: SystemUiOverlayStyle(
              statusBarColor: Color(0x4f5a55ca),
              statusBarBrightness: Brightness.light,
              statusBarIconBrightness: Brightness.light),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Color(0xff5A55CA),
            elevation: 16,
            backgroundColor: Colors.white,
            unselectedItemColor: Colors.grey),
        textTheme: TextTheme(
          bodyText1: TextStyle(
              fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      darkTheme: ThemeData(
        primarySwatch: Colors.deepPurple,
        floatingActionButtonTheme:
            FloatingActionButtonThemeData(backgroundColor: Color(0xff5A55CA)),
        scaffoldBackgroundColor: HexColor('333739'),
        appBarTheme: AppBarTheme(
          titleSpacing: 20,
          centerTitle: false,
          iconTheme: IconThemeData(color: Colors.white),
          elevation: 0.0,
          backgroundColor: HexColor('333739'),
          backwardsCompatibility: false,
          titleTextStyle: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
          systemOverlayStyle: SystemUiOverlayStyle(
              statusBarColor: HexColor('333739'),
              statusBarBrightness: Brightness.light,
              statusBarIconBrightness: Brightness.light),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Color(0xff5A55CA),
            elevation: 16,
            backgroundColor: HexColor('333739'),
            unselectedItemColor: Colors.grey),
        textTheme: TextTheme(
          bodyText1: TextStyle(
              fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
        ),
      ),
      // initialRoute:'home_screen' ,
      home: LaunchScreen(),
      routes: {
        'launch_screen': (context) => LaunchScreen(),
        'login_screen': (context) => LoginScreen(),
        'crate_account': (context) => SignUpScreen(),
        'forgot_password': (context) => ForgetPasswordScreen(),
        'home_screen': (context) => HomeScreen(),
        'distributors_screen': (context) => DistributorsScreen(),
        'customer_screen': (context) => CustomerScreen(),
        'category_home': (context) => CategoryHome(),
        'add_category': (context) => AddCategory(),
        'add_product': (context) => AddProductScreen(),
        'show_order': (context) => OrderShow(),
        'bill_add_num1': (context) => AddBillNum1(),
        'bill_add_num2': (context) => AddBillNum2(),
        'bill_add_num3': (context) => AddBillNum3(),
        'bill_add_num4': (context) => AddBillNum4(),
        'bills_show': (context) => BillsShow(),
        'setting_screen': (context) => SettingScreen(),
        'finish_bills': (context) => FinishBills(),
        // 'profile_screen': (context) => ProfileScreen(),
        'change_password': (context) => ChangePassword(),
      },
    );
  }
}
