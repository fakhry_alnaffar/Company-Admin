import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_auth_controller.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/admin.dart';
import 'package:youth_flutter_final_project/models/dashboard_items.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/screen/profile/profile_screen.dart';
import 'package:youth_flutter_final_project/widgets/component.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  String name = '';
  late User _user;
  int _countCustomer = 0;
  int _countEmployee = 0;
  int _countOrder = 0;
  int _countProduct = 0;
  int _countBills = 0;
  List<DashboardItem> items = <DashboardItem>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    listValue();
    getItemCount();
    getUserName();
    _user = FbAuthController().user;
    getUserName();
    setState(() {
      getUserName();
      listValue();
      getItemCount();
      _user = FbAuthController().user;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance!.removeObserver(this);
    listValue();
    getItemCount();
    getUserName();
    _user = FbAuthController().user;

    setState(() {
      listValue();
      getItemCount();
      getUserName();
      _user = FbAuthController().user;
    });

    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //do your stuff
      listValue();
      getItemCount();
      getUserName();
      _user = FbAuthController().user;

      setState(() {
        listValue();
        getItemCount();
        getUserName();
        _user = FbAuthController().user;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    items = <DashboardItem>[
      DashboardItem(
          title: 'الموزعين',
          counterAndType: '${_countEmployee.toString()} موزع  ',
          icon: Icons.directions_car,
          iconColor: Color(0xff30e208)),
      DashboardItem(
          title: 'الزبائن',
          counterAndType: '${_countCustomer.toString()} زبون  ',
          icon: Icons.person,
          iconColor: Color(0xff6e59e5)),
      DashboardItem(
        title: 'الطلبيات',
        counterAndType: '${_countOrder.toString()} طلبية  ',
        icon: Icons.shopping_cart_outlined,
        iconColor: Colors.redAccent,
      ),
      DashboardItem(
          title: 'المنتجات',
          counterAndType: '${_countProduct.toString()} منتج  ',
          icon: Icons.list_outlined,
          iconColor: Color(0xffF26950)),
      DashboardItem(
          title: 'الفواتير ',
          counterAndType: '${_countBills.toString()} مهمة ',
          icon: Icons.request_page,
          iconColor: Color(0xff61bf21)),
      DashboardItem(
          title: 'الحساب',
          counterAndType: 'اعددات الحساب',
          icon: Icons.settings_applications,
          iconColor: Color(0xff2393e0)),
    ];
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // leading: Builder(
        //   builder: (BuildContext context) {
        //     return IconButton(
        //       onPressed: () {
        //         Scaffold.of(context).openDrawer();
        //         print(FbFireStoreController().readCustomer().length);
        //       },
        //       tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
        //       icon: Container(
        //         margin: EdgeInsets.symmetric(vertical: 5),
        //         child: Image(
        //           image: AssetImage(
        //             "images/aa.png",
        //           ),
        //         ),
        //       ),
        //     );
        //   },
        // ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(5),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'لوحة التحكم',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      // drawer: ClipRRect(
      //   borderRadius: BorderRadius.only(
      //       bottomRight: Radius.circular(150.0),
      //       topRight: Radius.circular(150)),
      //   child: Container(
      //     color: Color(0xff5A55CA),
      //     width: 210,
      //     child: Drawer(
      //       child: Container(
      //         decoration: BoxDecoration(
      //           gradient: LinearGradient(
      //             begin: AlignmentDirectional.topStart,
      //             end: AlignmentDirectional.bottomEnd,
      //             colors: [
      //               Color(0xff5A55CA),
      //               Color(0xff5A55CA),
      //               Color(0x995a55ca),
      //             ],
      //           ),
      //         ),
      //         child: ListView(
      //           clipBehavior: Clip.antiAliasWithSaveLayer,
      //           physics: NeverScrollableScrollPhysics(),
      //           padding: EdgeInsets.symmetric(vertical: 10),
      //           primary: true,
      //           children: [
      //             UserAccountsDrawerHeader(
      //               decoration: BoxDecoration(
      //                 borderRadius: BorderRadius.circular(0),
      //                 gradient: LinearGradient(
      //                   begin: AlignmentDirectional.topStart,
      //                   end: AlignmentDirectional.bottomEnd,
      //                   colors: [
      //                     Color(0xff5A55CA),
      //                     Color(0xff5A55CA),
      //                     Color(0x995a55ca),
      //                   ],
      //                 ),
      //               ),
      //               accountName: Text(name),
      //               accountEmail: Text('${_user.email}'),
      //               currentAccountPicture: CircleAvatar(
      //                 backgroundColor: Colors.white,
      //                 child: Icon(
      //                   Icons.person_outline_rounded,
      //                   size: 40,
      //                   color: Color(0xff5A55CA),
      //                 ),
      //               ),
      //               arrowColor: Colors.black,
      //             ),
      //             Divider(
      //               color: Colors.grey,
      //               endIndent: 5,
      //               indent: 0,
      //               thickness: 1.5,
      //             ),
      //             SizedBox(
      //               height: 5,
      //             ),
      //             ListTile(
      //               // isThreeLine: true,
      //               dense: true,
      //               selected: false,
      //               leading: Icon(
      //                 Icons.settings_applications,
      //                 size: 30,
      //                 color: Colors.white,
      //               ),
      //               title: Container(
      //                   margin: EdgeInsetsDirectional.only(bottom: 0),
      //                   padding: EdgeInsetsDirectional.only(bottom: 0),
      //                   child: Text(
      //                     'الاعدادات',
      //                     style: TextStyle(fontSize: 18, color: Colors.white),
      //                   )),
      //             ),
      //             SizedBox(height: SizeConfig().scaleHeight(10)),
      //             ListTile(
      //               dense: true,
      //               selected: false,
      //               leading: Icon(
      //                 Icons.login_outlined,
      //                 size: 30,
      //                 color: Colors.white,
      //               ),
      //               title: Container(
      //                   margin: EdgeInsetsDirectional.only(bottom: 0),
      //                   child: Text(
      //                     'تسجيل الخروج',
      //                     style: TextStyle(fontSize: 18, color: Colors.white),
      //                   )),
      //               onTap: () {
      //                 FbAuthController().signOut(context);
      //               },
      //             ),
      //           ],
      //         ),
      //       ),
      //     ),
      //   ),
      // ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: GridView.builder(
                    padding: EdgeInsetsDirectional.only(top: 50),
                    physics: BouncingScrollPhysics(),
                    clipBehavior: Clip.antiAlias,
                    itemCount: items.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: SizeConfig().scaleHeight(24),
                        crossAxisSpacing: SizeConfig().scaleHeight(24),
                        childAspectRatio: 171 / 171,
                        mainAxisExtent: 171),
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          if (index == 0) {
                            print('distributors');
                            Navigator.pushNamed(context, 'distributors_screen');
                          } else if (index == 1) {
                            print('customer');
                            Navigator.pushNamed(context, 'customer_screen');
                          } else if (index == 2) {
                            Navigator.pushNamed(context, 'show_order');
                            print('order');
                          } else if (index == 3) {
                            Navigator.pushNamed(context, 'category_home');
                            print('product');
                          } else if (index == 4) {
                            Navigator.pushNamed(context, 'bills_show');
                            print('bills');
                          } else if (index == 5) {
                            // Navigator.push(context, MaterialPageRoute(
                            //     builder: (context) => ProfileScreen(),
                            //     ),);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ProfileScreen()));
                            print('setting');
                          }
                        },
                        child: homeWidget(
                          icon: items[index].icon,
                          counterAndType: items[index].counterAndType,
                          tittle: items[index].title,
                          iconColor: items[index].iconColor,
                        ),
                      );
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> getCustomerCount() async {
    int count = await FbFireStoreController().readCustomerCount();
    setState(() {
      _countCustomer = count;
    });
  }

  Future<void> getEmployeeCount() async {
    int count = await FbFireStoreController().readEmployeeCount();
    setState(() {
      _countEmployee = count;
    });
  }

  Future<void> getOrderCount() async {
    int count = await FbFireStoreController().readOrderCount();
    setState(() {
      _countOrder = count;
    });
  }

  Future<void> getProductCount() async {
    int count = await FbFireStoreController().readProductCount();
    setState(() {
      _countProduct = count;
    });
  }

  Future<void> getBillsCount() async {
    int count = await FbFireStoreController().readBillsCount();
    setState(() {
      _countBills = count;
    });
  }

  Future<void> getUserName() async {
    String namee = await FbFireStoreController().getAdminName();
    setState(() {
      name = namee;
      print('Name ' + name);
    });
  }

  void listValue() {
    DashboardItem(
        title: 'الموزعين',
        counterAndType: '${_countEmployee.toString()} موزع  ',
        icon: Icons.directions_car,
        iconColor: Color(0xff30e208));
    DashboardItem(
        title: 'الزبائن',
        counterAndType: '${_countCustomer.toString()} زبون  ',
        icon: Icons.person,
        iconColor: Color(0xff6e59e5));
    DashboardItem(
      title: 'الطلبيات',
      counterAndType: '${_countOrder.toString()} طلبية  ',
      icon: Icons.shopping_cart_outlined,
      iconColor: Colors.redAccent,
    );
    DashboardItem(
        title: 'المنتجات',
        counterAndType: '${_countProduct.toString()} منتج  ',
        icon: Icons.list_outlined,
        iconColor: Color(0xffF26950));
    DashboardItem(
        title: 'الفواتير ',
        counterAndType: '${_countBills.toString()} مهمة ',
        icon: Icons.request_page,
        iconColor: Color(0xff61bf21));
    DashboardItem(
        title: 'الحساب',
        counterAndType: 'اعددات الحساب',
        icon: Icons.settings_applications,
        iconColor: Color(0xff2393e0));
  }

  void getItemCount() {
    getCustomerCount();
    getEmployeeCount();
    getOrderCount();
    getProductCount();
    getBillsCount();
  }

  Admin getAdmin() {
    Admin admin = Admin();
    admin.name = name;
    admin.email = _user.email.toString();
    return admin;
  }
}
