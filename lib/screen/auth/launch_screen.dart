
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:youth_flutter_final_project/firebase/fb_auth_controller.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
class LaunchScreen extends StatefulWidget {
  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      var route;
      if(FbAuthController().isLoggedIn){
        route = 'home_screen';
      }else{
        route = 'login_screen';
      }
      Navigator.pushReplacementNamed(context, route);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFCE1E9FD),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                      margin: EdgeInsets.only(bottom: SizeConfig().scaleHeight(0)),
                      child:Image(image:  AssetImage("images/l.png"),)
                  ),
                  SizedBox(
                    width: SizeConfig().scaleWidth(20),
                  ),
                  Text(
                    'Bio Clean',
                    style: TextStyle(
                        fontSize: SizeConfig().scaleTextFont(30),
                        fontWeight: FontWeight.bold),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
