import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/category.dart';
import 'package:youth_flutter_final_project/models/product.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/screen/product/add_product.dart';
import 'package:youth_flutter_final_project/utils/helpers.dart';
import 'package:youth_flutter_final_project/widgets/component.dart';

class ShowProductCategory extends StatefulWidget {
  final Category category;

  ShowProductCategory(this.category);

  @override
  _ShowProductCategoryState createState() => _ShowProductCategoryState();
}

class _ShowProductCategoryState extends State<ShowProductCategory>  with Helpers{
  String categoryName = '';
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'الاصناف',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: StreamBuilder<QuerySnapshot>(
                  // stream: FbFireStoreController().readCustomProduct(widget.category.path),
                   stream: FbFireStoreController().readCustomProduct(widget.category.path),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      print(documents);
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 0),
                        child: ListView.separated(
                          shrinkWrap: false,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                          padding: EdgeInsetsDirectional.only(top: 50),
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: showProduct(
                                  documents: documents,
                                  index: index,
                                  context: context,
                                  title: documents[index].get('nameProduct'),
                                  subtitle:'Price    '+ documents[index]
                                      .get('price')
                                      .toString(),
                                  image: documents[index].get('image')),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AddProductScreen(
                                        product: getProduct(documents[index]),
                                      ),
                                    ));
                              },
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: SizeConfig().scaleHeight(14),
                            );
                          },
                          itemCount: documents.length,
                        ),
                      );
                    } else {
                      return Center(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.warning_amber_outlined,
                                color: Colors.grey,
                                size: 50,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'No items found',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  }),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, 'add_product');
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xff5A55CA),
      ),
    );
  }


  Future<void> delete({required String path}) async {
    bool status = await FbFireStoreController().deleteProduct(path: path);
    if (status) {
      showSnackBar(context: context, content: 'News Deleted Successfully');
    }
  }

  Product getProduct(DocumentSnapshot snapshot) {
    Product product = Product();
    product.path = snapshot.id;
    product.nameProduct = snapshot.get('nameProduct');
    product.nameCategory = snapshot.get('nameCategory');
    product.categoryId = snapshot.get('categoryId');
    product.price = snapshot.get('price');
    product.image = snapshot.get('image');
    return product;
  }


}
