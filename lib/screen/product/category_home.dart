import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/category.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/screen/product/show_product_category.dart';
import 'package:youth_flutter_final_project/utils/helpers.dart';
import 'package:youth_flutter_final_project/widgets/component.dart';

import 'edit_category.dart';

class CategoryHome extends StatefulWidget {
  @override
  _CategoryHomeState createState() => _CategoryHomeState();
}

class _CategoryHomeState extends State<CategoryHome> with Helpers {
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pushNamed(context,'home_screen');
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(5),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'التصنيفات',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: StreamBuilder<QuerySnapshot>(
                      stream: FbFireStoreController().readCategory(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else if (snapshot.hasData &&
                            snapshot.data!.docs.isNotEmpty) {
                          List<QueryDocumentSnapshot> data =
                              snapshot.data!.docs;
                          return GridView.builder(
                              padding: EdgeInsetsDirectional.only(top: 50),
                              shrinkWrap: false,
                              physics: BouncingScrollPhysics(),
                              clipBehavior: Clip.antiAlias,
                              itemCount: data.length,
                              gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  mainAxisSpacing:
                                  SizeConfig().scaleHeight(24),
                                  crossAxisSpacing:
                                  SizeConfig().scaleHeight(24),
                                  childAspectRatio: 171 / 171,
                                  mainAxisExtent: 171),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onLongPress: (){
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => EditCategory(
                                            category:getCategory(data[index]) ,

                                          ),
                                        ));
                                  },
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ShowProductCategory(
                                                    getCategory(data[index]))));
                                  },
                                  child: homeWidget(
                                      iconVisible: true,
                                      titleVisible: true,
                                      icon: Icons.category_outlined,
                                      counterAndType: '',
                                      tittle: data[index].get('categoryName'),
                                      iconColor: Colors.purple,
                                      iconDelete: IconButton(
                                          onPressed: () async{
                                            await delete(path: data[index].id);
                                            print(data[index].id);
                                          },
                                          icon: Icon(
                                            Icons.delete,
                                            color: Colors.red.shade800,
                                          ))),
                                );
                              });
                        } else {
                          return Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.warning,
                                  size: 80,
                                  color: Colors.grey.shade500,
                                ),
                                Text(
                                  'No Data',
                                  style: TextStyle(
                                      color: Colors.grey.shade500,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ],
                            ),
                          );
                        }
                      })),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //TODO:
          Navigator.pushNamed(context, 'add_category');
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xff5A55CA),
      ),
    );
  }

  Future<void> delete({required String path}) async {
    bool status = await FbFireStoreController().deleteCategory(path: path);
    if (status) {
      List<String> ids = await FbFireStoreController().getProductId(path);
      for(int i = 0; i < ids.length; i++){
        await FbFireStoreController().deleteProduct(path: ids[i]);
      }
      showSnackBar(context: context, content: 'Category Deleted Successfully');
    }
  }

  Category getCategory(DocumentSnapshot snapshot) {
    Category category = Category();
    category.categoryName = snapshot.get('categoryName');
    category.path=snapshot.id;
    return category;
  }
}
