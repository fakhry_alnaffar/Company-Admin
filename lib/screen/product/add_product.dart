import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/firebase/firebase_storage.dart';
import 'package:youth_flutter_final_project/models/product.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/utils/helpers.dart';
import 'package:youth_flutter_final_project/widgets/app_text_filed.dart';

class AddProductScreen extends StatefulWidget {
  final Product? product;

  AddProductScreen({this.product});

  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> with Helpers {
  String categoryName = '';
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage = '';
  String? category;
  late TextEditingController _nameController;
  late TextEditingController _categoryNameController;
  late TextEditingController _priceController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController =
        TextEditingController(text: widget.product!=null ? widget.product!.nameProduct:'');
    _categoryNameController =
        TextEditingController(text: widget.product!=null ? widget.product!.nameCategory:'');
    _priceController = TextEditingController(text: widget.product!=null ? widget.product!.price:'');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _categoryNameController.dispose();
    _priceController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'اضافة صنف',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: SizeConfig().scaleHeight(40),
                        ),
                        GestureDetector(
                          onTap: () {
                            pickImage();
                          },
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Color(0xff5A55CA),
                            ),
                            child: widget.product != null
                                ? pickedImage2 != null
                                ? Image.file(
                              File(pickedImage2!.path),
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              widget.product!.image,
                              fit: BoxFit.cover,
                            )
                                : pickedImage2 != null
                                ? Image.file(
                              File(pickedImage2!.path),
                              fit: BoxFit.cover,
                            )
                                : Icon(
                              Icons.add_photo_alternate_outlined,
                              color: Colors.white,
                              size: 50,
                            ),
                          ),
                        ),
                        SizedBox(height: 40,),
                        AppTextFiled(
                          labelText: 'اسم الصنف',
                          controller: _nameController,
                          prefix: Icons.title,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        GestureDetector(
                          onTap: (){
                            // showBottomSheet2();
                            print('wfas');

                          },
                          child: InkWell(
                            child: AppTextFiled(
                              showCursor: false,
                              readOnly: true,
                              labelText: 'اسم التصنيف',
                              controller: _categoryNameController,
                              prefix: Icons.category_rounded,
                              onTap: (){
                                // print('wfas');
                                showBottomSheet2();
                              },
                            ),
                            onTap: (){
                              print('wfas');
                            },
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'السعر',
                          controller: _priceController,
                          prefix: Icons.price_change,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () async {
                            print('clicked');
                            bool s = await performSave();
                            if (s) {
                              Navigator.pop(context);
                            }
                          },
                          child: Text(
                            widget.product != null ? 'تعديل' : 'اضافة',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20),
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> performSave() async {
    if (checkData()) {
      if (widget.product == null) {
        await save();
      } else {
        await update();
      }
      return true;
    }
    return false;
  }

  bool checkData() {
    if (_nameController.text.isNotEmpty &&
        _categoryNameController.text.isNotEmpty &&
        _priceController.text.isNotEmpty&&
        selectedImage != '') {
      return true;
    }
    showSnackBar(
        context: context, content: 'Enter Required Data!', error: true);
    return false;
  }
  void clear() {
    _nameController.text = '';
    _priceController.text = '';
    _categoryNameController.text = '';
  }
  Future<void> save() async {
    bool status = await FbFireStoreController().createProduct(product: product);
    if (status) {
      late String message =
      widget.product != null ? 'Update Successfully' : 'Added Successfully';

      showSnackBar(context: context, content: message, error: !status);
      clear();
    }
  }

  Product get product {
    Product product = Product();
    product.nameProduct = _nameController.text;
    product.nameCategory = _categoryNameController.text;
    product.categoryId = category!;
    product.price = _priceController.text;
    product.image = selectedImage;
    return product;
  }


  void showBottomSheet2() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FbFireStoreController().readCategory(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('categoryName')),
                      value: documents[index].id,
                      groupValue: category,
                      onChanged: (String? value) {
                        if (value != null)
                          setState(
                                () {
                              category = value;
                              _categoryNameController.text = documents[index].get('categoryName');
                              Navigator.pop(context);
                            },
                          );
                      },
                    );
                  },
                ),
              );
            } else {
              return Container();
            }
          },
        );
      },
    );
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('Uploading image...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController()
        .uploadImagee(File(pickedImage!.path), 'products');
  }

  Future<void> update() async {
    bool status = await FbFireStoreController()
        .updateProduct(path: widget.product!.path, product: product);
    if (status) {
      late String message =
      widget.product != null ? 'Update Successfully' : 'Added Successfully';
      showSnackBar(context: context, content: message, error: !status);
      Navigator.pop(context);
    }
  }
}
