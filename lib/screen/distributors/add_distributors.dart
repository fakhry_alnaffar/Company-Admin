import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:youth_flutter_final_project/firebase/fb_auth_controller.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/firebase/firebase_storage.dart';
import 'package:youth_flutter_final_project/models/distributor.dart';
import 'package:youth_flutter_final_project/preferences/app_preferences.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/utils/helpers.dart';
import 'package:youth_flutter_final_project/widgets/app_text_filed.dart';

class AddDistributorsScreen extends StatefulWidget {
  final Distributor? distributor;

  AddDistributorsScreen({required this.distributor});

  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<AddDistributorsScreen> with Helpers {
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage =
      'https://lh3.googleusercontent.com/proxy/BTzVU2nd4jldbPBcL-ZPRZnj0-fpEc-o3kWT2MqDBh_dXOmmjXjrlCaefzBMY8Z70WpOwqX2dE_6MdF-ajMXy7YbTm73KRn5XbfkvWmU4yuKgb3GYy0mhipyyN8V53-EJgUrAbNVkhwnvhuoafa0SHCcNJ1k';
  late TextEditingController _nameController;
  late TextEditingController _carNumberController;
  late TextEditingController _typeCarController;
  late TextEditingController _mobileNumberController;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late bool isPassword = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController(
        text: widget.distributor != null ? widget.distributor!.name : '');
    _carNumberController = TextEditingController(
        text: widget.distributor != null ? widget.distributor!.carNumber : '');
    _typeCarController = TextEditingController(
        text: widget.distributor != null ? widget.distributor!.typeCar : '');
    _mobileNumberController = TextEditingController(
        text:
            widget.distributor != null ? widget.distributor!.mobileNumber : '');
    _emailController = TextEditingController(
        text: widget.distributor != null ? widget.distributor!.email : '');
    _passwordController =
        TextEditingController(text: widget.distributor != null ? ' ' : '');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _carNumberController.dispose();
    _typeCarController.dispose();
    _mobileNumberController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            '${widget.distributor != null ? 'تعديل بيانات الموزع' : 'اضافة موزع'}',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          Visibility(
            visible: widget.distributor != null,
            child: IconButton(
                onPressed: () {
                  deleteDistributorFromAuth();
                  deleteDistributor();
                },
                icon: Icon(
                  Icons.delete_outline,
                  color: Colors.white,
                )),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: SizeConfig().scaleHeight(10),
                        ),
                        GestureDetector(
                          onTap: () {
                            pickImage();
                          },
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xff5A55CA),
                            ),
                            child: widget.distributor != null
                                ? pickedImage2 != null
                                    ? Image.file(
                                        File(pickedImage2!.path),
                                        fit: BoxFit.cover,
                                      )
                                    : Image.network(
                                        widget.distributor!.image,
                                        fit: BoxFit.cover,
                                      )
                                : pickedImage2 != null
                                    ? Image.file(
                                        File(pickedImage2!.path),
                                        fit: BoxFit.cover,
                                      )
                                    : Icon(
                                        Icons.add_photo_alternate_outlined,
                                        color: Colors.white,
                                        size: 50,
                                      ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        AppTextFiled(
                          labelText: 'اسم الموزع',
                          controller: _nameController,
                          prefix: Icons.person,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'رقم سيارة التوزيع',
                          // isEnabled: false,
                          controller: _carNumberController,
                          prefix: Icons.format_list_numbered_sharp,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.text,
                          labelText: 'نوع السيارة',
                          // isEnabled: false,
                          controller: _typeCarController,
                          prefix: Icons.directions_car_rounded,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'رقم الجوال',
                          controller: _mobileNumberController,
                          prefix: Icons.phone,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          // enabled: widget.distributor != null ? false : true,
                          readOnly: widget.distributor != null ? true : false,
                          showCursor: widget.distributor != null ? false : true,
                          textInputType: TextInputType.emailAddress,
                          labelText: 'البريد الالكتروني',
                          controller: _emailController,
                          prefix: Icons.email,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          // enabled: widget.distributor != null ? false : true,
                          readOnly: widget.distributor != null ? true : false,
                          showCursor: widget.distributor != null ? false : true,
                          labelText: 'كلمة المرور',
                          controller: _passwordController,
                          obscureText: isPassword,
                          prefix: Icons.lock,
                          suffix: isPassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          functionSuffixPressed: () {
                            setState(() {
                              isPassword = !isPassword;
                            });
                          },
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () {
                            signUp();
                          },
                          child: Text(
                            widget.distributor != null ? 'تعديل' : 'اضافة',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool checkData() {
    if (_carNumberController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _mobileNumberController.text.isNotEmpty &&
        _nameController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        _typeCarController.text.isNotEmpty) {
      return true;
    } else {
      showSnackBar(
          context: context,
          content: 'One or more input are empty',
          error: true);
      return false;
    }
  }

  Future<void> signUp() async {
    if (checkData()) {
      if (widget.distributor != null) {
        await updateDistributor();
      } else {
        await createAccountProces();
      }
    }
  }

  Future<void> createAccountProces() async {
    await FirebaseAuth.instance.signOut();
    bool state = await FbAuthController().createAccount(context,
        email: _emailController.text, password: _passwordController.text);
    if (state) {
      await FbFireStoreController().addDistributor(distributor: distributor);
      await FirebaseAuth.instance.signOut();
      await FbAuthController().signIn(context,
          email: AppPreferences().getEmail(),
          password: AppPreferences().getPassword());
      Navigator.pop(context);
    }
  }

  Future<void> updateDistributor() async {
    bool state = await FbFireStoreController().updateDistributor(
        path: widget.distributor!.path, distributor: distributor);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> deleteDistributor() async {
    bool state = await FbFireStoreController()
        .deleteDistributor(path: widget.distributor!.path);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> deleteDistributorFromAuth() async {
    var state = await FbAuthController().deleteUser();
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('Uploading image...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController()
        .uploadImagee(File(pickedImage!.path), 'products');
  }

  Distributor get distributor {
    Distributor distributor = Distributor();
    distributor.name = _nameController.text;
    distributor.email = _emailController.text;
    distributor.carNumber = _carNumberController.text;
    distributor.mobileNumber = _mobileNumberController.text;
    distributor.typeCar = _typeCarController.text;
    distributor.image = selectedImage;
    return distributor;
  }
}
