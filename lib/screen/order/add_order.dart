import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/order.dart';
import 'package:youth_flutter_final_project/models/team.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/utils/helpers.dart';

class AddOrder extends StatefulWidget {
  final Order? order;

  AddOrder({required this.order});

  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<AddOrder> with Helpers {
  late TextEditingController _nameController;
  String? distributor;
  List<Team> teem = <Team>[];
  List<Team> employeeName = <Team>[];
  List<Team> teemSelected = <Team>[];
  String? name;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController();
    getCustomerName();
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            '${widget.order != null ? 'تعديل مهمة' : 'اضافة مهمة'}',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          Visibility(
              visible: widget.order!=null,
              child: IconButton(onPressed: (){deleteOrder();}, icon: Icon(Icons.delete_outline, color: Colors.white,)))
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: SizeConfig().scaleHeight(40),
                        ),
                        InkWell(
                          onTap: () {
                            showBottomSheet2();
                          },
                          child: Container(
                            width: double.infinity,
                            height: 80,
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(0xff5A55CA), width: 2),
                                color: Color(0xffF0F4FD),
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0),
                                  topLeft: Radius.circular(10.0),
                                )),
                            child: teemSelected.length < 0
                                ? Text('data')
                                : Container(
                              margin: EdgeInsetsDirectional.only(
                                  start: 0, end: 0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                physics: BouncingScrollPhysics(),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                padding: EdgeInsets.zero,
                                child: Row(
                                  children: [
                                    Container(
                                        margin:
                                        EdgeInsetsDirectional.only(
                                            start: 10, end: 10),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.person,
                                              color: Color(0xff5A55CA),
                                              size: 25,
                                            ),
                                            SizedBox(
                                              width: SizeConfig()
                                                  .scaleWidth(10),
                                            ),
                                            Text(
                                              'أسم الموزع',
                                              textDirection:
                                              TextDirection.rtl,
                                              style: TextStyle(
                                                  color:
                                                  Color(0xff5A55CA),
                                                  fontSize: 16),
                                            ),
                                            SizedBox(
                                              width: SizeConfig()
                                                  .scaleWidth(50),
                                            ),
                                            widget.order != null? Center(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                                children: [
                                                  CircleAvatar(
                                                    child: Icon(
                                                      Icons.person,
                                                      color: Colors.white,
                                                      size: 20,
                                                    ),
                                                    backgroundColor:
                                                    Color(0xff5A55CA),
                                                    radius: 20,
                                                  ),

                                                  Text(
                                                    '${widget.order != null ?  widget.order!.name.toString(): name.toString()}',
                                                    textDirection:
                                                    TextDirection.rtl,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                        FontWeight.w500,
                                                        fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                            ) : name != null ? Column(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              children: [
                                                CircleAvatar(
                                                  child: Icon(
                                                    Icons.person,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                  backgroundColor:
                                                  Color(0xff5A55CA),
                                                  radius: 20,
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(name!.toString())
                                              ],
                                            ) : Text(''),

                                          ],
                                        )),
                                    SizedBox(
                                      width: SizeConfig().scaleWidth(10),
                                    ),
                                    // ListView.separated(
                                    //   padding: EdgeInsetsDirectional.zero,
                                    //   physics: BouncingScrollPhysics(),
                                    //   shrinkWrap: true,
                                    //   itemCount: employeeName.length,
                                    //   scrollDirection: Axis.horizontal,
                                    //   itemBuilder: (context, index) {
                                    //     return widget.order != null ? Column(
                                    //       mainAxisSize: MainAxisSize.max,
                                    //       mainAxisAlignment:
                                    //           MainAxisAlignment.center,
                                    //       crossAxisAlignment:
                                    //           CrossAxisAlignment.center,
                                    //       children: [
                                    //         CircleAvatar(
                                    //           child: Icon(
                                    //             Icons.person,
                                    //             color: Colors.white,
                                    //             size: 20,
                                    //           ),
                                    //           backgroundColor:
                                    //               Color(0xff5A55CA),
                                    //           radius: 20,
                                    //         ),
                                    //         SizedBox(
                                    //           height: 5,
                                    //         ),
                                    //         Text(widget.order!.marketName.toString())
                                    //       ],
                                    //     ) : Text('');
                                    //   },
                                    //   separatorBuilder:
                                    //       (BuildContext context,
                                    //           int index) {
                                    //     return SizedBox(
                                    //       width:
                                    //           SizeConfig().scaleWidth(10),
                                    //     );
                                    //   },
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        InkWell(
                          onTap: () {
                            showBottomSheet();
                          },
                          child: Container(
                            width: double.infinity,
                            height: 80,
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(0xff5A55CA), width: 2),
                                color: Color(0xffF0F4FD),
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0),
                                  topLeft: Radius.circular(10.0),
                                )),
                            child: employeeName.length < 0
                                ? Text('data')
                                : Container(
                              margin: EdgeInsetsDirectional.only(
                                  start: 0, end: 0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                physics: BouncingScrollPhysics(),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                padding: EdgeInsets.zero,
                                child: Row(
                                  children: [
                                    Container(
                                        margin:
                                        EdgeInsetsDirectional.only(
                                            start: 10, end: 10),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.shop_outlined,
                                              color: Color(0xff5A55CA),
                                              size: 25,
                                            ),
                                            SizedBox(
                                              width: SizeConfig()
                                                  .scaleWidth(10),
                                            ),
                                            Text(
                                              ' اسم المحل',
                                              textDirection:
                                              TextDirection.rtl,
                                              style: TextStyle(
                                                  color:
                                                  Color(0xff5A55CA),
                                                  fontSize: 16),
                                            ),
                                            // Text(
                                            //   '  ${widget.order!=null?widget.order!.marketName:''}',
                                            // ),
                                            widget.order!=null ?                                           ListView.separated(
                                              padding: EdgeInsetsDirectional.zero,
                                              physics: BouncingScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount: widget.order!.marketName.length,
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder: (context, index) {
                                                return Column(
                                                  mainAxisSize: MainAxisSize.max,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                                  children: [
                                                    CircleAvatar(
                                                      child: Icon(
                                                        Icons.shop_outlined,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                      backgroundColor:
                                                      Color(0xff5A55CA),
                                                      radius: 20,
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      //here work to show shop name
                                                      widget.order!.marketName[index],
                                                      textDirection:
                                                      TextDirection.ltr,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                          FontWeight.w500,
                                                          fontSize: 16
                                                      ),

                                                    ),
                                                  ],
                                                );
                                              },
                                              separatorBuilder:
                                                  (BuildContext context,
                                                  int index) {
                                                return SizedBox(
                                                  width:
                                                  SizeConfig().scaleWidth(10),
                                                );
                                              },
                                            ) : Text('')
                                          ],
                                        )),
                                    SizedBox(
                                      width: SizeConfig().scaleWidth(20),
                                    ),
                                    ListView.separated(
                                      padding: EdgeInsetsDirectional.zero,
                                      physics: BouncingScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: teemSelected.length,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, index) {

                                        return Column(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          children: [
                                            CircleAvatar(
                                              child: Icon(
                                                Icons.shop_outlined,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                              backgroundColor:
                                              Color(0xff5A55CA),
                                              radius: 20,
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              //here work to show shop name
                                              teemSelected[index].name,
                                              textDirection:
                                              TextDirection.ltr,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 16
                                              ),

                                            ),
                                          ],
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context,
                                          int index) {
                                        return SizedBox(
                                          width:
                                          SizeConfig().scaleWidth(10),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () {
                            save();
                          },
                          child: Text(
                            '${widget.order != null ? 'تعديل مهمة' : 'اضافة مهمة'}',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void showBottomSheet2() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FbFireStoreController().readDistributor(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('name')),
                      value: documents[index].get('email').toString(),
                      groupValue: distributor,
                      onChanged: (String? value) {
                        if (value != null)
                          setState(
                                () {
                              distributor = value;
                              _nameController.text =
                                  documents[index].get('name');
                              print(documents[index].get('name'));
                              name = documents[index].get('name');
                              // widget.order!.name.toString();
                            },
                          );
                        Navigator.pop(context);
                      },
                    );
                  },
                ),
              );
            } else {
              return Container();
            }
          },
        );
      },
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
        barrierColor: Colors.black.withOpacity(0.25),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        context: context,
        builder: (context) {
          return Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: StatefulBuilder(
                      builder: (BuildContext context,
                          void Function(void Function()) setState) {
                        return ListView.builder(
                          itemCount: teem.length,
                          itemBuilder: (context, index) {
                            return CheckboxListTile(
                              title: Text(teem[index].name),
                              value: teem[index].states,
                              onChanged: (status) {
                                setState(() {
                                  teem[index].states = status!;
                                });
                                print(status);
                              },
                            );
                          },
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                        Size(double.infinity, SizeConfig().scaleHeight(60)),
                        primary: Color(0xff5A55CA),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          teemSelected.clear();
                          for (Team teem in teem) {
                            if (teem.states) {
                              teemSelected.add(teem);
                            }
                          }
                        });
                        Navigator.pop(context);
                      },
                      child: Text(
                        'حسنا',
                        style:
                        TextStyle(fontSize: SizeConfig().scaleTextFont(20)),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }

  bool checkData() {
    if (_nameController.text.isNotEmpty && teemSelected.length != 0) {
      return true;
    } else {
      showSnackBar(
          context: context,
          content: 'One or more input are empty',
          error: true);
      return false;
    }
  }

  Future<void> save() async {
    if (checkData()) {
      if (widget.order != null) {
        await updateOrder();
      } else {
        await addOrder();
      }
    }
  }

  Future<void> deleteOrder() async{
    bool state = await FbFireStoreController().deleteOrder(path: widget.order!.path);
    if(state){
      Navigator.pop(context);
    }
  }

  Future<void> addOrder() async {
    bool state = await FbFireStoreController().addOrder(order: order);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> updateOrder() async {
    bool state = await FbFireStoreController()
        .updateOrder(path: widget.order!.path, order: order);
    if (state) {
      Navigator.pop(context);
    }
  }

  Order get order {
    Order order = Order();
    order.name = _nameController.text;
    order.email = distributor!;
    order.done = '0';
    for (int i = 0; i < teemSelected.length; i++) {
      print(teemSelected[i].name);
      order.marketName.add(teemSelected[i].name);
    }
    return order;
  }

  Future<void> getCustomerName() async {
    List<String> names = await FbFireStoreController().getMarketName();
    for (int i = 0; i < names.length; i++) {
      print(names.length);
      setState(() {
        teem.add(Team(name: names[i], states: false));
      });
    }
  }

}
//0xff5A55CA
