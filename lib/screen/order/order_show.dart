import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/order.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/widgets/component.dart';

import 'add_order.dart';

class OrderShow extends StatefulWidget {
  @override
  _OrderShowState createState() => _OrderShowState();
}

class _OrderShowState extends State<OrderShow> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pushNamed(context, 'home_screen');
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'الطلبيات',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: StreamBuilder<QuerySnapshot>(
                  stream: FbFireStoreController().readOrder(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 0),
                        child: ListView.separated(
                          padding: EdgeInsetsDirectional.only(top: 50),
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: showItem(
                                icon: documents[index].get('done') == '0' ? Icons.task : Icons.done,
                                  documents: documents,
                                  index: index,
                                  context: context,
                                  title: documents[index].get('name'),
                                  subtitle: documents[index]
                                      .get('marketName')
                                      .toString(),
                              date: documents[index]
                                  .get('created')
                                  .toString(),
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AddOrder(
                                        order: getOrder(documents[index]),
                                      ),
                                    ));
                              },
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: SizeConfig().scaleHeight(14),
                            );
                          },
                          itemCount: documents.length,
                          shrinkWrap: false,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                        ),
                      );
                    } else {
                      return Center(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.warning_amber_outlined,
                                color: Colors.grey,
                                size: 50,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'NO ORDERS',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  }),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddOrder(
                  order: null,
                ),
              ));
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xff5A55CA),
      ),
    );
  }

  Order getOrder(DocumentSnapshot snapshot) {
    Order order = Order();
    order.name = snapshot.get('name');
    order.marketName = snapshot.get('marketName');
    order.created = snapshot.get('created');
    order.path = snapshot.id;
    return order;
  }
}
