import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_auth_controller.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/bills.dart';
import 'package:youth_flutter_final_project/preferences/app_preferences.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/widgets/app_text_filed.dart';

class AddBillNum4 extends StatefulWidget {
  const AddBillNum4({Key? key}) : super(key: key);

  @override
  _AddBillNum3State createState() => _AddBillNum3State();
}

class _AddBillNum3State extends State<AddBillNum4> {
  late TextEditingController _totalPriceController;
  late TextEditingController _paidPriceController;
  late TextEditingController _residualPriceController;
  late User _user;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _user = FbAuthController().user;
    _totalPriceController = TextEditingController(text: AppPreferences().getTotalPrice().toString());
    _paidPriceController = TextEditingController(text: AppPreferences().getAmountPaid().toString());
    _residualPriceController = TextEditingController(text: (AppPreferences().getTotalPrice() - AppPreferences().getAmountPaid()).toString());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _totalPriceController.dispose();
    _paidPriceController.dispose();
    _residualPriceController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'الفاتورة',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: SizeConfig().scaleHeight(40),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'اجمالي المبلغ',
                          readOnly: true,
                          showCursor: false,
                          controller: _totalPriceController,
                          prefix: Icons.price_change_rounded,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'المبلع المدفوع',
                          readOnly: true,
                          showCursor: false,
                          controller: _paidPriceController,
                          prefix: Icons.price_change_rounded,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'المبلغ المتبقي',
                          readOnly: true,
                          showCursor: false,
                          controller: _residualPriceController,
                          prefix: Icons.price_change_rounded,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () async{
                            await addBill();
                          },
                          child: Text(
                            'انهاء',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> addBill() async{
    bool state = await FbFireStoreController().addBill(bill: bill);
    if(state){
      Navigator.pushReplacementNamed(context,'finish_bills');
    }
  }

  Bill get bill {
    Bill bill = Bill();
    bill.marketName = AppPreferences().getMarketName();
    bill.marketId = AppPreferences().getMarketId();
    bill.product = AppPreferences().getCategory();
    bill.amountPaid = AppPreferences().getAmountPaid().toString();
    bill.totalPrice = AppPreferences().getTotalPrice().toString();
    bill.remainingAmount = (AppPreferences().getTotalPrice() - AppPreferences().getAmountPaid()).toString();
    bill.email =_user.email!;
    bill.done = '0';
    return bill;
  }
}