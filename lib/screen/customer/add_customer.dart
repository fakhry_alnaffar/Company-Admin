import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/models/customers.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/utils/helpers.dart';
import 'package:youth_flutter_final_project/widgets/app_text_filed.dart';

class AddCustomerScreen extends StatefulWidget {
  final Customers? customer;

  AddCustomerScreen({required this.customer});

  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<AddCustomerScreen> with Helpers {
  late TextEditingController _nameController;
  late TextEditingController _addressController;
  late TextEditingController _shopName;
  late TextEditingController _phone;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController(
        text: widget.customer != null ? widget.customer!.name : '');
    _addressController = TextEditingController(
        text: widget.customer != null ? widget.customer!.address : '');
    _shopName = TextEditingController(
        text: widget.customer != null ? widget.customer!.marketName : '');
    _phone = TextEditingController(
        text: widget.customer != null ? widget.customer!.phoneNumber : '');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _addressController.dispose();
    _shopName.dispose();
    _phone.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            '${widget.customer != null ? 'تعديل بيانات الزبون' : 'اضافة زبون'}',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          Visibility(visible: widget.customer!=null,child: IconButton(onPressed: (){deleteCustomer();}, icon: Icon(Icons.delete_outline, color: Colors.white,)))
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: SizeConfig().scaleHeight(40),
                        ),
                        AppTextFiled(
                          labelText: 'اسم الزبون',
                          controller: _nameController,
                          prefix: Icons.person,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          labelText: 'اسم المحل',
                          controller: _shopName,
                          prefix: Icons.title_rounded,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.streetAddress,
                          labelText: 'العنوان',
                          controller: _addressController,
                          prefix: Icons.map_rounded,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.number,
                          labelText: 'رقم الجوال',
                          controller: _phone,
                          prefix: Icons.phone,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () {
                            save();
                          },
                          child: Text(
                            widget.customer != null ? 'تعديل' : 'اضافة',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool checkData() {
    if (_nameController.text.isNotEmpty &&
        _addressController.text.isNotEmpty &&
        _phone.text.isNotEmpty &&
        _shopName.text.isNotEmpty) {
      return true;
    } else {
      showSnackBar(
          context: context,
          content: 'One or more input are empty',
          error: true);
      return false;
    }
  }

  Future<void> save() async {
    if (checkData()) {
      if (widget.customer != null) {
        await updateCustomer();
      } else {
        await addCustomer();
      }
    }
  }

  Future<void> addCustomer() async {
    bool state = await FbFireStoreController().addCustomer(customer: customer);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> updateCustomer() async {
    bool state = await FbFireStoreController()
        .updateCustomer(path: widget.customer!.path, customer: customer);
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> deleteCustomer() async{
    List<String> ids = await FbFireStoreController().getBillId(widget.customer!.path);
    if(ids.length > 0){
      showSnackBar(context: context, content: 'You can not delete this customer', error: true);
    }else{
      bool state = await FbFireStoreController().deleteCustomer(path: widget.customer!.path);
      if(state){
        Navigator.pop(context);
      }
    }

  }

  Customers get customer {
    Customers customer = Customers();
    customer.name = _nameController.text;
    customer.address = _addressController.text;
    customer.marketName = _shopName.text;
    customer.phoneNumber = _phone.text;
    return customer;
  }
}
