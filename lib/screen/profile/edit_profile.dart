import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:youth_flutter_final_project/firebase/fb_auth_controller.dart';
import 'package:youth_flutter_final_project/firebase/fb_firestore.dart';
import 'package:youth_flutter_final_project/firebase/firebase_storage.dart';
import 'package:youth_flutter_final_project/models/admin.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';
import 'package:youth_flutter_final_project/widgets/app_text_filed.dart';

// ignore: must_be_immutable
class EditProfile extends StatefulWidget {
  Admin admin;
  EditProfile(this.admin);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage = '';
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  bool isNameEdit = false;
  bool isPhoneEdit = false;
  bool isEmailEdit = false;
  bool isImageEdit = false;
  IconData iconEditImage = Icons.edit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController(text:  widget.admin.name,);
    _emailController = TextEditingController(text: widget.admin.email,);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            '  بيانات الحساب',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(height: 40),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 0),
                                color: Colors.white.withOpacity(0.16),
                                blurRadius: 6,
                              ),
                            ],
                          ),
                          child: Stack(
                            alignment: AlignmentDirectional.bottomEnd,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(80),
                                  border:
                                  Border.all(width: 8, color: Colors.white),
                                ),
                                child: Container(
                                  clipBehavior: Clip.antiAlias,
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Colors.deepOrange),
                                  child: widget.admin != null
                                      ? pickedImage2 != null
                                      ? Image.file(
                                    File(pickedImage2!.path),
                                    fit: BoxFit.cover,
                                  )
                                      : Image.network(
                                    widget.admin.image,
                                    fit: BoxFit.cover,
                                  )
                                      : pickedImage2 != null
                                      ? Image.file(
                                    File(pickedImage2!.path),
                                    fit: BoxFit.cover,
                                  )
                                      : Icon(
                                    Icons.add_photo_alternate_outlined,
                                    color: Colors.white,
                                    size: 50,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsetsDirectional.only(
                                    bottom: 12, end: 5, top: 0, start: 0),
                                child: Container(
                                  // padding: EdgeInsetsDirectional.only(bottom: 50),
                                  child: CircleAvatar(
                                    backgroundColor: Color(0xff5A55CA),

                                    radius: 13,
                                    child: IconButton(
                                      padding: EdgeInsetsDirectional.zero,
                                      onPressed: () {
                                        isImageEdit = !isImageEdit;
                                        if (isImageEdit == true) {
                                          pickImage();
                                          setState(() {
                                            pickImage();
                                            iconEditImage =
                                                Icons.check_outlined;
                                            print('go to check');

                                          });
                                        }
                                        if (isImageEdit == false) {
                                          setState(() {
                                            print('back to edit');
                                            iconEditImage = Icons.edit;
                                            ///method update image
                                            updateImage();
                                          });
                                        }
                                      },
                                      icon: Icon(iconEditImage,color: Colors.white,size: 18,),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(40),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.text,
                          labelText: 'اسم المستخدم',
                          readOnly: !isNameEdit,
                          showCursor: isNameEdit,
                          controller: _nameController,
                          prefix: Icons.person,
                          suffix: isNameEdit ? Icons.check : Icons.edit,
                          functionSuffixPressed: () async{
                            setState(() {
                              isNameEdit = !isNameEdit;
                              if (isNameEdit == true) {
                                ///update name method
                              }

                              if (isNameEdit == false) {
                                ///update name method
                                updateName();
                                print('update');
                              }
                            });
                          },
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        AppTextFiled(
                          textInputType: TextInputType.emailAddress,
                          labelText: 'البريد الالكتروني',
                          controller: _emailController,
                          prefix: Icons.email,
                          readOnly: !isEmailEdit,
                          showCursor: isEmailEdit,
                          // suffix: isEmailEdit ? Icons.check : Icons.edit,
                          functionSuffixPressed: () {
                            setState(() {
                              isEmailEdit = !isEmailEdit;
                              if (isEmailEdit == true) {
                                /// UPDATE method EMAIL
                              }
                            });
                          },
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        // ElevatedButton(
                        //   style: ElevatedButton.styleFrom(
                        //     minimumSize: Size(
                        //         double.infinity, SizeConfig().scaleHeight(60)),
                        //     primary: Color(0xff5A55CA),
                        //     shape: RoundedRectangleBorder(
                        //       borderRadius: BorderRadius.circular(10),
                        //     ),
                        //   ),
                        //   onPressed: () async {
                        //     print('clicked');
                        //     await updateName();
                        //   print('name update ');
                        //   print('id ${widget.id}');
                        //   },
                        //   child: Text(
                        //    'تعديل',
                        //     style: TextStyle(
                        //         fontSize: SizeConfig().scaleTextFont(20),
                        //         fontWeight: FontWeight.w600),
                        //   ),
                        // ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('Uploading image...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });

    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController()
        .uploadImagee(File(pickedImage!.path), 'profile');
  }


  Future<void> updateImage() async{
    await FbFireStoreController().updateAdminImage(path: widget.admin.path, image: selectedImage);
  }

  Future<void> updateName() async{
    await FbFireStoreController().updateAdminName(path: widget.admin.path, name: _nameController.text);
  }

}
