import 'package:flutter/material.dart';
import 'package:youth_flutter_final_project/models/admin.dart';
import 'package:youth_flutter_final_project/screen/profile/edit_profile.dart';
import 'package:youth_flutter_final_project/widgets/component.dart';
import 'package:youth_flutter_final_project/models/dashboard_items.dart';
import 'package:youth_flutter_final_project/responsive/size_config.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  List<DashboardItem> items = <DashboardItem>[
    DashboardItem(
        title: 'تغير كلمة المرور',
        counterAndType: '',
        icon: Icons.lock_outline_rounded,
        iconColor: Color(0xff6109ba)),
    DashboardItem(
        title: 'تعديل بيانات الحساب',
        counterAndType: '',
        icon: Icons.edit_outlined,
        iconColor: Color(0xffe0a507)),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pushNamed(context,'home_screen');
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(5),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'الاعدادت',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Center(
                  child: GridView.builder(
                      padding: EdgeInsetsDirectional.only(top: 50),
                      physics: BouncingScrollPhysics(),
                      clipBehavior: Clip.antiAlias,
                      itemCount: items.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: SizeConfig().scaleHeight(24),
                          crossAxisSpacing: SizeConfig().scaleHeight(24),
                          childAspectRatio: 171 / 171,
                          mainAxisExtent: 171),
                      itemBuilder: (context, index) {
                        return InkWell(
                          child: Center(
                            child: homeWidget(
                              icon: items[index].icon,
                              counterAndType: items[index].counterAndType,
                              tittle: items[index].title,
                              iconColor: items[index].iconColor,
                            ),
                          ),
                          onTap: (){
                            if (index == 0) {
                              print('CHANGE');
                              Navigator.pushNamed(context, 'change_password');
                            }else if (index == 1) {
                              print('EDIT');
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile(Admin()),));
                            }
                          },
                        );
                      }),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

}
